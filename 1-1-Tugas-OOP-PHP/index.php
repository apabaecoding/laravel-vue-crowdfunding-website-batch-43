<?php

trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi()
    {
        echo "{$this->nama} sedang lari cepat {$this->keahlian}";
    }
}

abstract class Fight{
    use Hewan;
    public $attackPower;
    public $defencePower;

    public function serang($hewan)
    {
        echo "{$this->nama}sedang menyerang {$hewan->nama}";

        $hewan->diserang($this);
    }

    public function diserang($hewan)
    {
        echo "{$this->nama}sedang diserang {$hewan->nama}";

        $this->darah = $this->darah - ($hewan->attackPower / $this->defencePower);
    }

    protected function getInfo()
    {
        echo "<br>";
        echo "Nama : {$this->nama}";
        echo "<br>";
        echo "Darah : {$this->darah}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
        echo "Attack Power : {$this->attackPower}";
        echo "<br>";
        echo "Defence Power : {$this->defencePower}";
        echo "<br>";

        $this->atraksi();

    }

    abstract public function getInfoHewan();
}

class Harimau extends Fight{
    public function_construct($nama){
        $this->nama = $nama;
        $this->darah = 50;
        $this->jumlahkaki = 4;
        $this->keahlian = "Sedang menyerang elang";
        $this->attackPower = 120;
        $this->defencePower = 40;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Harimau";
        $this->getInfo();
    }
}

class Spasi{
    public static function tampilkan(){
        echo "<br>";
        echo "================================";
        echo "<br>";
    }
}

class Elang extends Fight{
    public function_construct($nama){
        $this->nama = $nama;
        $this->darah = 50;
        $this->jumlahkaki = 2;
        $this->keahlian = "Sedang menyerang harimau";
        $this->attackPower = 110;
        $this->defencePower = 60;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Elang";
        $this->getInfo();
    }
}

$harimau = new Harimau("Harimau");

$harimau->getInfoHewan();

Spasi::tampilkan();

$elang = new Elang("elang");

$elang->getInfoHewan();

Spasi::tampilkan();

$elang->diserang($harimau);
Spasi::tampilkan();
$harimau->getInfoHewan();

$harimau->diserang($elang);
Spasi::tampilkan();
$harimau->getInfoHewan();